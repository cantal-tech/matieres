Matieres
--------

Un objet matière et son formulaire permettant de lier des matières à un objet spip ex: produit
le formulaire permet de qualifier des liens:

ex:
- viscose: 30%
- coton: 60%

Ex: Pour sélectionner les vetements 100% viscose.
~~~~
<BOUCLE_produits(PRODUITS){type=vetements}{matieres.pourcentage=100}{matieres.nom=viscose}>
...
</BOUCLE>
~~~~


