<?php
/**
 * Created by PhpStorm.
 * User: Pierre
 * Date: 06/10/2018
 * Time: 22:33
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

function matieres_declarer_tables_interfaces($interfaces) {

    return $interfaces;
}

function matieres_declarer_tables_objets_sql($tables) {

    $tables['spip_matieres'] = array(
        'type' => 'matiere',
        'principale' => "oui",
        'page' => "",
        'field'=> array(
            "id_matiere"       => "bigint(21) NOT NULL",
            "titre"            => "tinytext NOT NULL DEFAULT ''",
        ),
        'key' => array(
            "PRIMARY KEY"        => "id_matiere",
        ),
        'champs_editables'  => array('titre'),
        'rechercher_champs' => array(
            'titre' => '5',
        ),
        'tables_jointures'  => array('matieres_liens'),
    );

    return $tables;
}

function matieres_declarer_tables_auxiliaires($tables) {

    $tables['spip_matieres_liens'] = array(
        'field' => array(
            "id_matiere"    => "bigint(21)  DEFAULT '0' NOT NULL",
            "id_objet"      => "bigint(21)  DEFAULT '0' NOT NULL",
            "objet"         => "varchar(25) DEFAULT ''  NOT NULL",
            "pourcentage"   => "smallint    DEFAULT '0' NOT NULL"
        ),
        'key' => array(
            "PRIMARY KEY"  => "id_matiere,id_objet,objet",
        )
    );

    return $tables;

}